import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CRUDTests {
    @Before
    public void setup(){
        new DB("jdbc:sqlite::memory:");
    }
    @Test
    public void can_connect_ok() throws SQLException{
        assertTrue(DB.conn instanceof Connection);
    }
    @Test(expected = Test.None.class)
    public void db_is_provisiones()throws SQLException{
        Statement checkSelect = DB.conn.createStatement();
        checkSelect.execute("SELECT * From restaurants;");
    }
    @Test
    public void create() throws SQLException{
        PreparedStatement insert = DB.conn.prepareStatement("INSERT INTO restaurants (name) VALUES (?);");
        insert.setString(1,"Nandos");
        insert.executeUpdate();
        Statement getResturant = DB.conn.createStatement();
        ResultSet results = getResturant.executeQuery("SELECT * FROM restaurants;");
        while(results.next()) {
            assertEquals(results.getString(2), "Nandos");
        }

    }
    @Test
    public void read() throws SQLException{
        PreparedStatement insert = DB.conn.prepareStatement("INSERT INTO restaurants (name) VALUES (?);");
        insert.setString(1,"Nandos");
        insert.executeUpdate();
        Statement getResturant = DB.conn.createStatement();
        ResultSet results = getResturant.executeQuery("SELECT * FROM restaurants WHERE restaurants.name IS 'Nandos';");
        while(results.next()) {
            assertEquals(results.getString(2), "Nandos");
        }

    }
    @Test
    public void update() throws SQLException{
        PreparedStatement insert = DB.conn.prepareStatement("INSERT INTO restaurants (name) VALUES (?);");
        insert.setString(1,"Nandos");
        insert.executeUpdate();
        insert.setString(1,"Five Guys");
        insert.executeUpdate();
        Statement getResturant = DB.conn.createStatement();

        ResultSet results = getResturant.executeQuery("SELECT * FROM restaurants WHERE restaurants.id IS 2;");
        
        while(results.next()) {
            assertEquals(results.getString(2), "Five Guys");
        }

    }
    @Test
    public void delete() throws SQLException{
        PreparedStatement insert = DB.conn.prepareStatement("INSERT INTO restaurants (name) VALUES (?);");
        insert.setString(1,"Nandos");
        insert.executeUpdate();
        insert.setString(1,"Five Guys");
        insert.executeUpdate();
        PreparedStatement delete = DB.conn.prepareStatement("DELETE FROM restaurants WHERE name = ?;");
        delete.setString(1,"Nandos");
        delete.executeUpdate();
        Statement getResturant = DB.conn.createStatement();
        
        ResultSet results = getResturant.executeQuery("SELECT * FROM restaurants;");
        
        while(results.next()) {
            assertEquals(results.getString(2), "Five Guys");
        }

    }
    @After
    public void teardown() throws SQLException{
        DB.conn.close();
    }
}
