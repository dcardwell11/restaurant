import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestsItem {
    @Test
    public void item_can_have_a_name(){
        Item wine = new Item("Red Wine", 8.99);
        assertEquals("Red Wine", wine.getName());
    }

    @Test
    public void item_can_have_a_price(){
        Item wine = new Item("Red Wine", 8.99);
        assertEquals(8.99, wine.getPrice(),0);
    }
}
