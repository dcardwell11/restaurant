import java.util.ArrayList;

public class Restaurant {
    private String name;
    private String imageUrl;
    private ArrayList<Menu> menus;

    public Restaurant(String name, String imageUrl){
        this.name = name;
        this.imageUrl = imageUrl;
        this.menus = new ArrayList<Menu>();
    }
    public String getName() {
        return name;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public ArrayList<Menu> getMenus() {
        return menus;
    }
    public void setMenu(Menu menu){
        this.menus.add(menu);
    }
    
    
}
