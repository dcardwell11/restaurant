import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestsMenu {
    @Test
    public void Menu_can_be_created(){
        Menu drinks = new Menu("Drinks Menu");
        assertEquals("Drinks Menu", drinks.getTitle());
    }
    @Test
    public void Menu_can_have_an_item_added(){
        Menu drinks = new Menu("Drinks Menu");
        Item wine = new Item("Red Wine", 8.99);
        drinks.setItems(wine);
        assertEquals(1, drinks.getItems().size());
    }

    @Test
    public void Menu_can_have_more_than_one_item_added(){
        Menu drinks = new Menu("Drinks Menu");
        Item wine = new Item("Red Wine", 8.99);
        Item wine1 = new Item("White Wine", 7.99);
        drinks.setItems(wine);
        drinks.setItems(wine1);
        assertEquals(2, drinks.getItems().size());
    }
   @Test
    public void can_return_item_from_Menu(){
        Menu drinks = new Menu("Drinks Menu");
        Item wine = new Item("Red Wine", 8.99);
        Item wine1 = new Item("White Wine", 7.99);
        drinks.setItems(wine);
        drinks.setItems(wine1);
        assertEquals("Drinks Menu", drinks.getTitle());
        assertEquals("Red Wine", drinks.getItems().get(0).getName());
        assertEquals("White Wine", drinks.getItems().get(1).getName());
    }
    @Test
    public void can_get_items_price_from_the_Menu(){
        Menu drinks = new Menu("Drinks Menu");
        Item wine = new Item("Red Wine", 8.99);
        Item wine1 = new Item("White Wine", 7.99);
        drinks.setItems(wine);
        drinks.setItems(wine1);
        assertEquals(8.99, drinks.getItems().get(0).getPrice(),0);
        assertEquals(7.99, drinks.getItems().get(1).getPrice(),0);    }
}

